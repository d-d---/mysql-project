# 도커를 활용한 mysql 설치하기

1. Docker를 설치한다.(윈도우) [[다운로드]](https://download.docker.com/win/stable/Docker%20Desktop%20Installer.exe)
<br>

2. 이 레포를 클론한다.
`git clone <repo url>`
<br>

3. 클론한 프로젝트 루트 디렉토리로 이동한다.
<br>

4. 도커 컴포즈로 실행한다.
`docker-compose up` 또는 `docker-compose up -d`
<br>

4-1. 도커 컴포즈가 실행 안될 시 (윈도우 기준)
 https://stackoverflow.com/questions/48066994/docker-no-matching-manifest-for-windows-amd64-in-the-manifest-list-entries
위 사이트의 답변대로 
docker 오른쪽 마우스 클릭 => settings 클릭 => Daemon 클릭 => Basic 클릭(Basic->Advanced로 변경되야함) => 아래 라인에서 계속;
=> 
{
  "registry-mirrors": [],
  "insecure-registries": [],
  "debug": true,
  "experimental": true
} 로 코드 입력 후 Apply
다시 4. 명령어를 입력

4.2. 4-1 후 4번 명령어 입력 후 에러 발생 시
Shared Drives(Daemon 같은 설정 창) 에서 C 드라이브 Shared에 Check
다시 4. 명령어를 입력

4.3 Shared Drives 가 없을 시
PowerShell콘솔을 관리자 권한으로 열기
Enable-WindowsOptionalFeature -Online -FeatureName Microsoft-Hyper-V -All 입력
다시 4. 명령어를 입력

5. 종료시 다른 cmd창을 열어 아래의 명령어를 입력한다.
`docker-compose down` 또는 `Ctrl + C`

# etc Command